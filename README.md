# GUX - Back

## Instrucciones

### 1. Construir imagenes `docker-compose -f local.yml build`
### 2. Levantar proyecto `docker-compose -f local.yml up`
### 3. Cargar data de prueba (en otra terminal) `docker-compose -f local.yml run --rm django python manage.py loaddata data_testings`
### 4. Acceder a los endpoints
Listar todas los pacientes por medio de HTTP GET desde localhost:8000/pacientes/

Se puede agregar filtros (query params)
* nro_rol (Tiene que ser el número exacto)
* nombre_paciente (Puede no ser el nombre exacto)
* fecha_alta_desde (formato yyyy-mm-dd)
* fecha_alta_hasta (formato yyyy-mm-dd)
* prevision (solo para opciones cod1 o cod2)
* estado (solo para opciones activo o inactivo)


