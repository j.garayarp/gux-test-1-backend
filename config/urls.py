"""Main URLs module."""

from django.urls import include, path

urlpatterns = [

    path('', include('gux.pacientes.urls')),

]
