"""Variables Constantes para app Paciente"""

PREVISION_1 = "cod1"
PREVISION_2 = "cod2"
PREVISION_CHOICES = ((PREVISION_1, "Ejemplo 1"), (PREVISION_2, "Ejemplo 2"))

ACTIVO = "activo"
INACTIVO = "inactivo"
ESTADO_CHOICES = (("activo", "Activo"), ("inactivo", "Inactivo"))