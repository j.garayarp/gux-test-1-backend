# Django REST Framework
from rest_framework import serializers

# Model
from gux.pacientes.models import Testings


class PacienteModelSerializer(serializers.ModelSerializer):
    """Pacientes model serializer."""

    class Meta:
        """Meta class."""
        model = Testings
        fields = '__all__'