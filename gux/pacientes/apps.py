"""Pacientes app."""

# Django
from django.apps import AppConfig


class PacientesAppConfig(AppConfig):
    """Pacientes app config."""
    default_auto_field = 'django.db.models.AutoField'
    name = 'gux.pacientes'
    verbose_name = 'Pacientes'