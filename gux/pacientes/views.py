# Django REST Framework
from gux.pacientes.models import Testings
from rest_framework.generics import ListAPIView

# Filters
from django_filters.rest_framework import DjangoFilterBackend
from gux.pacientes.filters import PacienteFilter

# Serializers
from gux.pacientes.serializers import PacienteModelSerializer

# Models
from gux.pacientes.models import Testings


class PacienteListView(ListAPIView):
    """Pacientes list view."""

    serializer_class = PacienteModelSerializer
    queryset = Testings.objects.all()
    ordering = ('-fecha_hospitalizacion')

    # Filters
    filter_backends = (DjangoFilterBackend,)
    filterset_class = PacienteFilter