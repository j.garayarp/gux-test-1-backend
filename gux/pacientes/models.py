"""Modelo de paciente."""

# Django
from django.db import models

# Constants
from gux.pacientes.constants import (
    PREVISION_CHOICES,
    PREVISION_1,
    ESTADO_CHOICES,
    ACTIVO,
)


class Testings(models.Model):
    """Modelo de paciente.
    
    """

    nro_rol = models.IntegerField()
    nombre_paciente = models.CharField(max_length=255)
    fecha_hospitalizacion = models.DateField()
    fecha_alta = models.DateField()
    codigo_prestacion = models.CharField(max_length=50)
    accion = models.CharField(max_length=4)
    prevision = models.CharField(
        max_length=10,
        choices=PREVISION_CHOICES,
        default=PREVISION_1
    )
    estado = models.CharField(
        max_length=10,
        choices=ESTADO_CHOICES,
        default=ACTIVO
    )