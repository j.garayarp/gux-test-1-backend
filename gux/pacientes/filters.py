# Django Filters
from django_filters import rest_framework as filters

# Models
from gux.pacientes.models import Testings

# Constants
from gux.pacientes.constants import PREVISION_CHOICES, ESTADO_CHOICES


class PacienteFilter(filters.FilterSet):
    nombre_paciente = filters.CharFilter(lookup_expr='icontains')
    prevision = filters.ChoiceFilter(choices=PREVISION_CHOICES)
    estado = filters.ChoiceFilter(choices=ESTADO_CHOICES)
    fecha_alta_desde = filters.DateFilter(field_name="fecha_alta", lookup_expr='gte')
    fecha_alta_hasta = filters.DateFilter(field_name="fecha_alta", lookup_expr='lte')

    class Meta:
        model = Testings
        fields = [
            'nro_rol', 
            'nombre_paciente', 
            'prevision', 
            'estado',
            'fecha_alta'
        ]