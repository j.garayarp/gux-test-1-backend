# Django
from django.urls import path

# Views
from gux.pacientes.views import PacienteListView

urlpatterns = [
    path('pacientes/', PacienteListView.as_view())
]
